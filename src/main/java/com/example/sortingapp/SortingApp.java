package com.example.sortingapp;

import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SortingApp {
    private static final Logger logger = LogManager.getLogger(SortingApp.class);

    public static void main(String[] args) {
        // Check if command-line arguments are provided
        if (args.length == 0) {
            logger.info("No arguments provided.");
            return;
        }

        // Parse command-line arguments as integers
        int[] numbers = new int[args.length];
        try {
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            logger.error("Invalid argument: Please provide integer values.", e);
            return;
        }

        // Sort the numbers in ascending order
        Arrays.sort(numbers);

        // Print the sorted numbers
        logger.info("Sorted numbers: {}", Arrays.toString(numbers));
    }
}
