package com.example.sortingapp;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;

import java.util.ArrayList;
import java.util.List;

public class TestAppender extends AbstractAppender {
    private final List<LogEvent> events = new ArrayList<>();

    public TestAppender() {
        super("TestAppender", null, null);
    }

    @Override
    public void append(LogEvent event) {
        events.add(event);
    }

    public List<LogEvent> getEvents() {
        return events;
    }
}

