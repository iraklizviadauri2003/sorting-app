package com.example.sortingapp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {
    private TestAppender testAppender;

    @Parameterized.Parameter(0)
    public String[] args;

    @Parameterized.Parameter(1)
    public String expectedOutput;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, "No arguments provided."},
                {new String[]{"5", "2", "10", "7"}, "Sorted numbers: [2, 5, 7, 10]"},
                {new String[]{"5", "2", "abc", "7"}, "Invalid argument: Please provide integer values."},
                {new String[]{}, "No arguments provided."},
                {new String[]{"5"}, "Sorted numbers: [5]"},
                {new String[]{"5", "2", "10", "7", "1", "4", "9", "3", "6", "8", "11"}, "Sorted numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]"}
        });
    }

    @Before
    public void setUp() {
        testAppender = new TestAppender();
        testAppender.start();
        ((Logger) LogManager.getRootLogger()).addAppender(testAppender);
    }

    @After
    public void tearDown() {
        ((Logger) LogManager.getRootLogger()).removeAppender(testAppender);
        testAppender.stop();
    }

    @Test
    public void testSortingApp() {
        SortingApp.main(args);

        List<LogEvent> events = testAppender.getEvents();
        assertEquals(1, events.size());
        assertEquals(expectedOutput, events.get(0).getMessage().getFormattedMessage());
    }
}
